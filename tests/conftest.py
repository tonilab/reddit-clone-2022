import pytest
from fastapi.testclient import TestClient
from v4_auth.main import app
from v4_auth.models import Base
from v4_auth.database import get_db

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

DATABASE_URL='postgresql://postgres:API@localhost:5432/sqlalchemy_test'

#  Running engine = 
database_engine=create_engine(DATABASE_URL)

# Template for the connection
TestingSessionTemplate = sessionmaker(autocommit=False, autoflush=False, bind=database_engine)

# Dependency : Create and close session on demand
def override_get_db():
    db = TestingSessionTemplate()
    try:
        yield db
    finally:
            db.close()    
            
@pytest.fixture()
def session():
    #Clean DB by 
    Base.metadata.drop_all(bind=database_engine)
     #Create 
    Base.metadata.create_all(bind=database_engine)
    app.dependency_overrides[get_db] = override_get_db

@pytest.fixture()
def create_user(client):
    user_credentioals = {"emal":"test.user@domain.lu", "password": "1234"}
#   post request to create a new user  
    res = client.post('/users', 
                      json=user_credentioals)
#   Adding password to response 
    new_user = res.json()
#  Adding password to the response
    new_user['passwort']= user_credentioals['password']
#   returning new_user including id , email, created_at and plain text password 
    return new_user



@pytest.fixture()
def client(session): 
    yield TestClient(app)
    
