from datetime import datetime
from pydantic import BaseModel, EmailStr

# UserPy
# Pyndamic schema for User Body validatiion
class UserPy(BaseModel):
    email: EmailStr
    password: str

# Pyndamic schema User Body Response
class User_Response(UserPy):
    id: int
    created_at: datetime
    class Config:
        orm_mode = True

#  # # # # Define the table BlokPost # # # 
# BlokPostPy

class BlogPostPy(BaseModel):
    title: str
    content: str
    writer_id: int
    

class BlogPost_Response(BlogPostPy):
    id: int
    created_at: datetime
    writer: User_Response
    class Config: 
        orm_mode = True
        