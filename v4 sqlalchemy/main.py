from typing import List
from database import get_db, database_engine
from fastapi import Depends, FastAPI, HTTPException, Response, status
from sqlalchemy.orm import Session
import models, schemas

# Create the table if don't exists yet
models.Base.metadata.create_all(bind=database_engine)

app= FastAPI()  # Run the server


#  Fetch all data from User table
@app.get('/users', response_model=List[schemas.User_Response])
def get_users(db: Session = Depends(get_db)):
    all_users = db.query (models.User).all()
    return all_users

#    for the blok post
@app.get('/posts', response_model=List[schemas.BlogPost_Response])
def get_posts(db: Session = Depends(get_db)):
    all_posts = db.query (models.BlogPost).all()
    return all_posts

#   
#  Fetch all data from User table
@app.get('/users/{uri_id}', response_model=schemas.User_Response)
def get_users(uri_id: int, db: Session = Depends(get_db)):
    corresponding_user = db.query(models.User).filter(models.User.id == uri_id).first()
    if not corresponding_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Not corresponding user was found with id: {uri_id}")
    return corresponding_user

# Create a new User
@app.post('/users', response_model=schemas.User_Response)
def create_user(user_body:schemas.UserPy, db:Session=Depends(get_db)):
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user

# Create an update  User# # # # # # # Tova sa moi gluposti # # # 
@app.post('/users', response_model=schemas.User_Response)
def update_user(user_body:schemas.UserPy, db:Session=Depends(get_db)):
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user