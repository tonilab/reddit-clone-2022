import psycopg2
from psycopg2.extras import RealDictCursor # add names of fields to cursor
from random import randrange
from fastapi import Body, FastAPI, HTTPException, Response, status
from pydantic import BaseModel  # Schema
from typing import Optional  # Optional fields

try:
    db_connection = psycopg2.connect(
        host='localhost',
        database= 'One - to Many',
        user= 'postgres',
        password = 'API',
        cursor_factory= RealDictCursor
    )
    cursor = db_connection.cursor()
    print('Database connection: successful')
except Exception as error:
    print('Database connection: failed')
    print('Error:', error)


class BlogPost(BaseModel):  # Pydantic schema for POST Body validation
    title: str
    content: str
# ME   published: bool = True
    writer_id: int



##### DATA #####: Array of Blog Posts (Local list)
my_posts = [ 
    {"id": 1, "title": "Welcome to our blog", "content": "This is the beginning..."},
    {"id": 2, "title": "Top 10 best activities in Luxembourg", "content": "Our list of..."}
]

# find and return the post with the a given i
def find_post(given_id):
    for post in my_posts:
        if post["id"] == given_id:
            return post

# Find and return the index of a specific post
def find_post_index(given_id):
    for index, post in enumerate(my_posts):
        if post["id"] == given_id:
            return index

        
###### FastAPI instance name ######
app = FastAPI()  



#######################################
#### "Path Operations" or "Routes"  ###
#######################################

# GET / :
@app.get("/")  # decorator
def hello():  # function
    return {"message": "Hello from my first API"}  # response



# GET /posts ***GET ALL BLOGPOSTS***
@app.get("/posts")
def get_posts():
    #Executing the SQL query
    cursor.execute("SELECT * FROM blogpost")
    # Retrieve all the posts (list)
    database_posts = cursor.fetchall()
    return {"data": database_posts}



# POST /posts  ***CREATE NEW BLOGPOST***
@app.post("/posts", status_code=status.HTTP_201_CREATED)   # POST /posts endpoint
def create_posts(new_post: BlogPost, response: Response):  # Use the pydantic schema class
    try:
    
    cursor.execute("INSERT INTO blogpost (title, content, writer_id) values (%s ,%s, %s) RETURNING *;", 
        (new_post.title, new_post.content, new_post.writer_id))
    
    post_dict = cursor.fetchone()
    db_connection.commit() # Save the changes to the Database
    # response.status_code = 201                           # Alternative solution to change the HTTP code
    return {"data": post_dict} 
except psycopg2.error.ForeinKeyViolation as err:
    raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
detail=f"ForeignKeyViolation with writer_id: {post_body.writer_id}")



#GET /posts/trending  ***GET LATEST BLOGPOST***
@app.get('/posts/trending')                                # Path order matters
def trending_posts():
    return {"data": my_posts[len(my_posts)-1]}



#GET /posts/{id_param}  ***GET POST WITH ID***
@app.get('/posts/{id_param}')                              # {id_param}  is the path parameter
def get_post(id_param: int, response: Response):           # id_param must be an integer                                       
    corresponding_post = find_post(id_param)               # use the function with converted URI
    if not corresponding_post:                             # If No corresponding post found throw an exception to the consumer
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    return {"data": corresponding_post}   



#DELETE /posts/{id_param}  ***DELETE POST WITH ID***
@app.delete('/posts/{id_param}')
def delete_post(id_param: int):
    # deleting post logic
    # 1. find the post
    corresponding_index = find_post_index(id_param)
    # Exception is not found
    if not corresponding_index:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    # 2. Remove element from array
    my_posts.pop(corresponding_index)
    return Response(status_code=status.HTTP_204_NO_CONTENT) # Best practice HTTP CODE 204 = not content response 



#PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put('/posts/{id_param}')
def replace_post(id_param: int, updated_post: BlogPost):
    # updating logic
    # 1. find the post
    corresponding_index = find_post_index(id_param)
    # 2. Exception if not found
    if not corresponding_index:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    # 3. Pydantic class to Dict
    post_dict = updated_post.dict()
    # 4 Inject ID
    post_dict["id"] = id_param
    # 5 Update my_posts element
    my_posts[corresponding_index] = post_dict
    return post_dict