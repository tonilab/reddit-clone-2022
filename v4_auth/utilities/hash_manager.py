from passlib.context import CryptContext

#Initialize the CrypContext
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

# define  "hash_pass" function : to generate a hash from a password string.
def hash_pass(password: str):
    return pwd_context.hash(password)

# define  function "verify_password"  : will take a string and a hashed password to see if they match. 
def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

